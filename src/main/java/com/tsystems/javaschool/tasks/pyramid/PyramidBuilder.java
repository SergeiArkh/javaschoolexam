package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int numOfRows = getNumOfRows(inputNumbers.size());

        if(inputNumbers.contains(null) || numOfRows == -1){     //if we cant get numOfRows or input contains null throw exception
            throw new CannotBuildPyramidException();
        } else {
            Collections.sort(inputNumbers);
            int numOfColumns = numOfRows * 2 - 1;
            int [][] result = new int[numOfRows][numOfColumns];
            int curIdx = 0;
            for (int i = 0; i < numOfRows; i++) {           //filling our array
                int k = numOfRows - i - 1;
                for (int j = 0; j <= i; j++) {
                    result[i][k] = inputNumbers.get(curIdx);
                    curIdx++;
                    k+=2;
                }
            }
            return result;
        }
    }
    // here we check if it's possible to build a pyramid using input array and if its possible, getting number of rows
    private int getNumOfRows(int numOfDigits){
        double h = (1+Math.sqrt(1 + 8 * numOfDigits))/4,
                h1 = (-1+Math.sqrt(1 + 8 * numOfDigits))/4;
        int numOfRows = -1;
        if(h == Math.ceil(h)){
            numOfRows = 2*(int)h - 1;
        }
        if(h1 == Math.ceil(h1)){
            numOfRows = 2*(int)h;
        }
        return numOfRows;
    }
}
